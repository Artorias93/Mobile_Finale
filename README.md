# My Friend Cthulhu

## Introduzione
My Friend Cthulhu è un gioco arcade per Android a tema Lovecraftiano, ispirato da giochi come Wario Ware e Cooking Mama.

Il gioco è stato sviluppato come progetto per il corso di Mobile del Master di Verona 2021/2022

Puoi scaricare l'APK ai seguenti link:  
* [Link OneDrive (Richiede l'accesso)](https://univr-my.sharepoint.com/:u:/g/personal/giovanni_carnovale_studenti_univr_it/ET1wrsUSDBZBtlMgcqUAUSgBqMraSi_anxO5BKrJekA-LQ?e=6K7gwb)  
* [Link Mega](https://mega.nz/file/P5QGnBTa#RmDnawI0JIc-kO0qftV-xHZYuNAcw1FkMDU0uyvA5-w)  

## Feature e Meccaniche

1. 16 Minigiochi originali, ognuno con 3 difficoltà con il loro bilanciamento, inclusi:
   1. Minigiochi che sfruttano giroscopio;
   2. Minigiochi basati sull'audio;
2. Modalità endless, con gestione degli High Score;
3. Modalità ricetta: evoca 8 creature giocando un mix di minigiochi unico per ognuno. Con gestione degli High Score.

## Descrizione Minigame
1. **Choose The Rune**: Identifica il testo di alcune rune pronunciate;
2. **Ritual Ninja**: Taglia gli ingredienti prima che cadano
3. **Stir The Cauldron**: Segui la ricetta, mescola alla giusta velocità, e non dimenticarti nessun ingrediente;
4. **Potion Brewer**: Mischia le pozioni correttamente per creare la pozione perfetta;
5. **Ouija Board**: Usa il giroscopio per guidare il cursore della tavola Ouija e scrivere la parola indicata
6. **Runes Drawing**: Disegna con precisione la runa a schermo;
7. **Necromancer Shop**: Se ti mancano ingredienti puoi sempre comprarli. Cerca di memorizzare la lista della spesa e metti nel carrello tutti gli ingredienti necessari;
8. **Darkness Bumper**: Combatti gli spiritelli, se stanno troppo tempo ti faranno perdere della vita;
9. **Balance Game**: Per creare la giusta miscela dovrai usare il giroscopio per tenere la bilancia equilibrata;
10. **Hit The Slime**: Sfoga la tua frustrazione cliccando più volte che puoi su uno Slime (Tranquillo, non si farà male);
11. **Escape The Dungeon**: Cerca di sfuggire dalle guardie che ti cercano in questo labirinto generato casualmente;
12. **Seal The Hound**: Non far scappare il mastino demoniaco, cerca di predire in che cella andrà e bloccalo prima che raggiunga l'estremità;
13. **Place The Candles**: Metti tutte le candele nei punti indicati della runa per perfezionare il tuo rito;
14. **Monsters Invaders**: I mostri stanno venendo ad attaccarti, usa la tua bacchetta magica per combatterli. Se sarai troppo lento il loro boss ti raggiungerà;
15. **Fight Cthulhu**: Puoi evocarli, ma non è detto che ti obbediranno. Impedisci a Cthulhu di rapire le principesse colpendo sui tentacoli rossi, gli altri servono solo a distrarti;
16. **The Mad Wizard**: Fatti strada per questo dungeon, evitando trappole, chierici arrabbiati, e paladini che vogliono fermarti.


## Crediti
* Alessandro Carattoli
* Andrea Alcaras
* Andrea Squillante
* Giovanni Carnovale  
